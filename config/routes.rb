Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'home#index'
  resources :home, only: :index
  resources :music, only: :index
  resources :projects, only: :index
end
