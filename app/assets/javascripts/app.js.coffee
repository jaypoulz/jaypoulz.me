$ ->
  class Site
    constructor: (@controller, @scenes) ->
      @offsetToName = {}
      for name, scene of @scenes
        @offsetToName[scene.offset()] = name
        @controller.addScene(scene)
        scene.on('enter', @update)

      @controller.scrollTo (offset) ->
        $("html").animate({scrollTop: offset})

      # Scroll Nav
      $("#nav-about-me").click =>
        @controller.scrollTo(@scenes.about)

      $("#nav-projects").click =>
        @controller.scrollTo(@scenes.projects)

      $("#nav-music").click =>
        @controller.scrollTo(@scenes.music)

      $("#nav-resume").click =>
        @controller.scrollTo(@scenes.resume)

      $("#nav-contact").click =>
        @controller.scrollTo(@scenes.contact)

    update: (event) =>
      @updateNav(event)

    updateNav: (event) =>
      offset = event.target.offset()
      name = @offsetToName[offset]
      unless offset % 1400
        $('.nav.navbar-nav > li').each (index, query) ->
          isActive = $(query).children()[0].id.indexOf(name) > 0
          $(query).toggleClass('active', isActive)

  # Scroll controller
  ctrlr = new ScrollMagic.Controller()

  # Scenes obj
  scenes = {}

  scenes.about = new ScrollMagic.Scene(
    duration: 700
    offset: 0
  )
  .setPin("#about-me")
  .addIndicators({name: "Section 1"})

  scenes.t1 = new ScrollMagic.Scene(
    duration: 700
    offset: 700
  )
  .addIndicators({name: "Transition 1"})

  scenes.projects = new ScrollMagic.Scene(
    duration: 700
    offset: 1400
  )
  .setPin("#projects")
  .addIndicators({name: "Section 2"})

  scenes.t2 = new ScrollMagic.Scene(
    duration: 700
    offset: 2100
  )
  .addIndicators("Transition 2")

  scenes.music = new ScrollMagic.Scene(
    duration: 700
    offset: 2800
  )
  .setPin("#music")
  .addIndicators({name: "Section 3"})

  scenes.t3 = new ScrollMagic.Scene(
    duration: 700
    offset: 3500
  )
  .addIndicators("Transition 3")

  scenes.resume = new ScrollMagic.Scene(
    duration: 700
    offset: 4200
  )
  .setPin("#resume")
  .addIndicators({name: "Section 4"})

  scenes.t4 = new ScrollMagic.Scene(
    duration: 700
    offset: 4900
  )
  .addIndicators("Transition 4")

  scenes.contact = new ScrollMagic.Scene(
    duration: 700
    offset: 5600
  )
  .setPin("#contact")
  .addIndicators({name: "Section 5"})

  scenes.t5 = new ScrollMagic.Scene(
    duration: 700
    offset: 6300
  )
  .addIndicators("Transition 5")

  # Make the new site obj to set scroll handlers
  window.site = new Site(ctrlr, scenes)
